import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Header from "./components/Header/Header";
import { routes } from "./Routes/routes";
<<<<<<< HEAD
import Header from "./components/Header/Header";
=======
>>>>>>> f353ad8a596dedbfe749e7b93853535e56778884

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          {routes.map(({ path, component }, index) => {
            return <Route key={index} path={path} element={component} />;
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
