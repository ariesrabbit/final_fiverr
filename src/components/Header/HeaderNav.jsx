import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setUserLogOut } from "../../redux/userSlice";
import { userLocal } from "../../services/local.service";

export default function HeaderNav() {
  let userInfo = useSelector((state) => state.userSlice.user);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let handleLogOut = () => {
    userLocal.remove();
    dispatch(setUserLogOut());
  };
  let renderUserNavDesktop = () => {
    if (userInfo) {
      return (
        <div>
          Hello - <button onClick={handleLogOut}>Logout</button>
        </div>
      );
    } else {
      return (
        <div
          onClick={() => {
            navigate("/login");
          }}
        >
          Login
        </div>
      );
    }
  };
  return <div>{renderUserNavDesktop()}</div>;
}
