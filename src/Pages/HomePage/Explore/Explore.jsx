import React from "react";

export default function Explore() {
  return (
    <div className="container max-widthContainer mt-4 px-4">
      <h2 className="mb-4 mt-10 ml-12 text-3xl font-semibold">
        Explore the marketplace
      </h2>
      <div className=" flex flex-wrap justify-center items-center font-mono">
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/graphics-design.d32a2f8.svg"
            alt="Graphics & Design"
            loading="lazy"
          />
          <span>Graphics &amp; Design</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/online-marketing.74e221b.svg"
            alt="Digital Marketing"
            loading="lazy"
          />
          <span>Digital Marketing</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/writing-translation.32ebe2e.svg"
            alt="Writing & Translation"
            loading="lazy"
          />
          <span>Writing &amp; Translation</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/video-animation.f0d9d71.svg"
            alt="Video & Animation"
            loading="lazy"
          />
          <span>Video &amp; Animation</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
            alt="Programming & Tech"
            loading="lazy"
          />
          <span>Music &amp; Audio</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/programming.9362366.svg"
            alt="Programming & Tech"
            loading="lazy"
          />
          <span>Programming &amp; Tech</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/business.bbdf319.svg"
            alt="Business"
            loading="lazy"
          />
          <span>Business</span>
        </div>
        <div className="flex justify-center items-center flex-col m-7">
          <img
            width="30%"
            src="https://fiverr-res.cloudinary.com/npm-assets/@fiverr/logged_out_homepage_perseus/apps/lifestyle.745b575.svg"
            alt="Lifestyle"
            loading="lazy"
          />
          <span>Lifestyle</span>
        </div>
      </div>
    </div>
  );
}
