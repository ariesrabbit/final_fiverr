import React, { useEffect, useState } from "react";
import { workServ } from "../../../services/work.service";
import MenuItems from "./MenuItems";
import styles from "./menu.module.css";

export default function MenuWork() {
  let [menuItems, setMenuItems] = useState([]);
  let fetchApi = async () => {
    try {
      let res = await workServ.getMenuWork();
      setMenuItems(res.data.content);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    fetchApi();
  }, []);

  return (
    <div className="flex justify-center my-3">
      <nav>
        <ul className={styles.menus}>
          {menuItems.map((menu) => {
            const depthLevel = 0;
            return (
              <MenuItems items={menu} key={menu.id} depthLevel={depthLevel} />
            );
          })}
        </ul>
      </nav>
    </div>
  );
}
