import MenuItems from "./MenuItems";
import styles from "./menu.module.css";

const Dropdown = ({ dsNhomChiTietLoai, dropdown, depthLevel }) => {
  depthLevel = depthLevel + 1;
  const dropdownClass = depthLevel > 1 ? styles.dropdown__submenu : "";
  return (
    <ul
      className={`${styles.dropdown} ${dropdownClass} ${
        dropdown ? styles.show : ""
      }`}
    >
      {dsNhomChiTietLoai?.map((submenu) => (
        <MenuItems
          items={submenu.tenNhom}
          key={submenu.id}
          depthLevel={depthLevel}
        />
      ))}
    </ul>
  );
};

export default Dropdown;
