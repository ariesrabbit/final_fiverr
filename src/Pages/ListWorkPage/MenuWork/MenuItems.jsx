import { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import Dropdown from "./Dropdown";
import styles from "./menu.module.css";

const MenuItems = ({ items, depthLevel }) => {
  const [dropdown, setDropdown] = useState(false);

  let ref = useRef();

  useEffect(() => {
    const handler = (event) => {
      if (dropdown && ref.current && !ref.current.contains(event.target)) {
        setDropdown(false);
      }
    };
    document.addEventListener("mousedown", handler);
    document.addEventListener("touchstart", handler);
    return () => {
      // Cleanup the event listener
      document.removeEventListener("mousedown", handler);
      document.removeEventListener("touchstart", handler);
    };
  }, [dropdown]);

  const onMouseEnter = () => {
    window.innerWidth > 960 && setDropdown(true);
  };

  const onMouseLeave = () => {
    window.innerWidth > 960 && setDropdown(false);
  };

  const closeDropdown = () => {
    dropdown && setDropdown(false);
  };

  return (
    <li
      className={styles.menu__items}
      ref={ref}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onClick={closeDropdown}
    >
      {items.url && items.dsNhomChiTietLoai ? (
        <>
          <button
            type="button"
            aria-haspopup="menu"
            aria-expanded={dropdown ? "true" : "false"}
            onClick={() => setDropdown((prev) => !prev)}
          >
            {window.innerWidth < 960 && depthLevel === 0 ? (
              items.tenLoaiCongViec
            ) : (
              <Link>{items.tenLoaiCongViec}</Link>
            )}

            {depthLevel > 0 && window.innerWidth < 960 ? null : depthLevel >
                0 && window.innerWidth > 960 ? (
              <span>&raquo;</span>
            ) : (
              <span className={styles.arrow} />
            )}
          </button>
          <Dropdown
            depthLevel={depthLevel}
            dsNhomChiTietLoai={items.dsNhomChiTietLoai}
            dropdown={dropdown}
          />
        </>
      ) : !items.url && items.dsNhomChiTietLoai ? (
        <>
          <button
            type="button"
            aria-haspopup="menu"
            aria-expanded={dropdown ? "true" : "false"}
            onClick={() => setDropdown((prev) => !prev)}
          >
            {items.tenLoaiCongViec}{" "}
            {depthLevel > 0 ? (
              <span>&raquo;</span>
            ) : (
              <span className={styles.arrow} />
            )}
          </button>
          <Dropdown
            depthLevel={depthLevel}
            dsNhomChiTietLoai={items.dsNhomChiTietLoai}
            dropdown={dropdown}
          />
        </>
      ) : (
        <Link>{items}</Link>
      )}
    </li>
  );
};

export default MenuItems;
