import React from "react";
import { Progress, Rate } from "antd";
import { Collapse } from "antd";
import { StarFilled, DownOutlined } from "@ant-design/icons";
import CommentJob from "./CommentJob";
const { Panel } = Collapse;
const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;
const totalReview = [
  { start: 5, total: 333, percent: 98 },
  { start: 4, total: 2, percent: 2 },
  { start: 3, total: 0, percent: 0 },
  { start: 2, total: 0, percent: 0 },
  { start: 1, total: 0, percent: 0 },
];

export default function AboutSeller({ detailJob }) {
  // {avatar,id,tenChiTietLoai,tenLoaiCongViec,tenNguoiTao,tenNhomChiTietLoai,--congViec<{danhGia,giaTien,hinhAnh,id,maChiTietLoaiCongViec,moTa,moTaNgan,nguoiTao,saoCongViec,tenCongViec}>}
  let renderToTalReview = () => {
    return totalReview.map((item, index) => {
      return (
        <div key={index} className="flex">
          <span className="w-1/5">{item.start} start</span>
          <Progress format={() => `(${item.total})`} percent={item.percent} />
        </div>
      );
    });
  };
  return (
    <main>
      {/* //Avatar  */}
      <main className="flex">
        <img
          src={detailJob.avatar}
          alt=""
          style={{ width: 50, height: 50, borderRadius: "100%" }}
        />
        <div>
          <p>{detailJob.tenNguoiTao}</p>
          <p>{detailJob.tenLoaiCongViec}</p>
          <Rate disabled value={detailJob.congViec?.saoCongViec} />
          <br />
          <button>Contact Me</button>
        </div>
      </main>
      {/* FAQ */}
      <main>
        <h1>FAQ</h1>
        <Collapse accordion>
          <Panel header="This is panel header 1" key="1">
            <p>{text}</p>
          </Panel>
          <Panel header="This is panel header 2" key="2">
            <p>{text}</p>
          </Panel>
          <Panel header="This is panel header 3" key="3">
            <p>{text}</p>
          </Panel>
          <Panel header="This is panel header 4" key="4">
            <p>{text}</p>
          </Panel>
        </Collapse>
      </main>
      {/* //Reviews  */}
      <main className="flex">
        <div className=" w-1/2">
          <h1>
            335 Review
            <span>
              <Rate disabled value={5} /> 5
            </span>
          </h1>
          {renderToTalReview()}
        </div>
        <div className="flex w-1/2 items-end justify-between">
          <div className="w-3/4">
            <p>Rating Breakdown</p>
            <p>seller coummunication level</p>
            <p>Recomend to a friend</p>
            <p>Service as described</p>
          </div>
          <div className="w-1/4">
            <div>
              5<StarFilled />
            </div>
            <div>
              5<StarFilled />
            </div>
            <div>
              5<StarFilled />
            </div>
          </div>
        </div>
      </main>
      <p>Fillters</p>
      <p>
        <span>introdustry</span>
        <span>
          All introdustry <DownOutlined />
        </span>
      </p>
      <CommentJob />
    </main>
  );
}
