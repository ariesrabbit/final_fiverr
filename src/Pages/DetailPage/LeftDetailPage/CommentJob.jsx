import moment from "moment";
import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { commentApis } from "../../../Apis/CommentApis";

export default function CommentJob() {
  let { maLoaiCongViec } = useParams();
  let [comment, setComment] = useState("");
  let [dataContent, setDataContent] = useState([]); //{avatar,ngayBinhLuan,noiDung,saoBinhLuan,tenNguoiBinhLuan}
  let userInfo = useSelector((state) => state.userSlice.user);

  useEffect(() => {
    (async () => {
      try {
        let newContentComment = await commentApis.getCommentByIdJob({
          maLoaiCongViec,
        });
        setDataContent(newContentComment);
      } catch (err) {
        console.log("err", err);
      }
    })();
  }, []);
  let renderContentComment = () => {
    return dataContent.map((item, index) => {
      return (
        <main key={index}>
          <div className="flex">
            <img
              style={{ height: 50, width: 50, borderRadius: "100%" }}
              src={item.avatar}
              alt=""
            />
            <span>{item.tenNguoiBinhLuan}</span>
            <span>{item.saoBinhLuan}</span>
          </div>
          <div>{item.noiDung}</div>
        </main>
      );
    });
  };
  let submitComment = () => {
    if (comment) {
      let dateNow = moment().toDate();
      let dataPostComment = {
        id: maLoaiCongViec,
        maNguoiBinhLuan: userInfo.user.id,
        maCongViec: maLoaiCongViec,
        ngayBinhLuan: moment(dateNow).format("DD/MM/YYYY"),
        noiDung: comment,
        saoBinhLuan: 5,
      };
      (async () => {
        try {
          await commentApis.postComment({ dataPostComment });
          let newContentComment = await commentApis.getCommentByIdJob({
            maLoaiCongViec,
          });
          console.log("newContentComment", newContentComment);
          setDataContent(newContentComment);
        } catch (err) {
          console.log("err", err);
        }
      })();
    } else {
      alert("bạn chưa có nội dung hình luận");
    }
  };
  return (
    <main>
      {renderContentComment()}
      <div className="flex">
        <img
          className="w-1/6"
          src=""
          alt=""
          style={{ height: 50, width: 50, borderRadius: "100%" }}
        />
        <input
          value={comment}
          onChange={(e) => {
            setComment(e.target.value);
          }}
          className="w-5/6 border-blue-400 border-2"
          type="text"
        />
      </div>
      <button onClick={submitComment} className="bg-red-500 text-white">
        Add Comment
      </button>
    </main>
  );
}
