import { Rate } from "antd";
import React from "react";
import styled from "../styles/LeftDetailPage.module.css";
import AboutSeller from "./AboutSeller";
export default function LeftDetailPage({ detailJob }) {
  // {avatar,id,tenChiTietLoai,tenLoaiCongViec,tenNguoiTao,tenNhomChiTietLoai,--congViec<{danhGia,giaTien,hinhAnh,id,maChiTietLoaiCongViec,moTa,moTaNgan,nguoiTao,saoCongViec,tenCongViec}>}

  return (
    <main className={`${styled.main}`}>
      <h1>{detailJob.congViec?.tenCongViec}</h1>
      {/* // Info  */}
      <main className="flex items-center border-b-2">
        <img
          src={detailJob.avatar}
          alt=""
          style={{ width: 50, height: 50, borderRadius: "100%" }}
        />
        <span>{detailJob.tenNguoiTao} | </span>
        <span>
          <Rate disabled value={detailJob.congViec?.saoCongViec} />
        </span>
        <span>({detailJob.congViec?.danhGia})</span>
      </main>
      <div>
        <img
          className="w-full h-96 object-cover"
          src={detailJob.congViec?.hinhAnh}
          alt=""
        />
      </div>
      {/* //About this gig*/}
      <main>
        <h1>About This Gig</h1>
        <p>Hello</p>
        <p>{detailJob.congViec?.moTa}</p>
      </main>
      {/* // About the seller  */}
      <AboutSeller detailJob={detailJob} />
    </main>
  );
}
