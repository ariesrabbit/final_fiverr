import DetailPage from "../Pages/DetailPage/DetailPage";
import HomePage from "../Pages/HomePage/HomePage";
import ListWorkPage from "../Pages/ListWorkPage/ListWorkPage";
import LoginAndRegister from "../Pages/LoginRegisterPage/LoginAndRegister";

export const routes = [
  {
    path: "/",
    component: <HomePage />,
  },
  {
    path: "/login",
    component: <LoginAndRegister />,
  },
  {
    path: "/work",
    component: <ListWorkPage />,
  },
  {
    path: "/detail/:maLoaiCongViec",
    component: <DetailPage />,
  },
];
