import { https } from "../urlConfig";

export const jobService = {
  getDetailJob: ({ maLoaiCongViec }) => {
    let uri = `/api/cong-viec/lay-cong-viec-chi-tiet/${maLoaiCongViec}`;
    return https.get(uri);
  },
};
