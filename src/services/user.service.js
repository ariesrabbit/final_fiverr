import { https } from "./urlConfig";

export const userServ = {
  postLogin: (dataLogin) => {
    let uri = "/api/auth/signin";
    return https.post(uri, dataLogin);
  },
  postSignUp: (dataSignUp) => {
    let uri = "/api/auth/signup";
    return https.post(uri, dataSignUp);
  },
};
